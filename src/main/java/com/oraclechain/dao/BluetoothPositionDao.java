package com.oraclechain.dao;

import com.oraclechain.entity.BluetoothPosition;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/9/8
 */
@Mapper
public interface BluetoothPositionDao {
    void insert(BluetoothPosition bluetoothPosition);
    void update(BluetoothPosition bluetoothPosition);
    void delete(Integer id);
    List<BluetoothPosition> getData(BluetoothPosition bluetoothPosition);
}
