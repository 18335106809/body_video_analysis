package com.oraclechain.test;

import com.oraclechain.socket.MoveThread;
import com.oraclechain.socket.MyWebSocketClient;
import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.HOGDescriptor;
import org.opencv.videoio.VideoCapture;

import javax.swing.*;
import java.io.File;


/**
 * @author liuhualong
 * @Description:
 * @date 2020/8/19
 */
public class walkFind {

//    static String url = "rtsp://admin:HikNZRSOK@192.168.0.48:554/Streaming/Channels/101?transportmode=unicast&profile=Profile_1";
    static String url = "rtsp://admin:zhgl8888@10.0.7.49:554/Streaming/Channels/101?transportmode=unicast";

    public static void main(String[] args) {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        videoRTSP();
    }

    public static void videoRTSP() {
        try {
            // 新建窗口
            JFrame cameraFrame = new JFrame("camera");
            cameraFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            cameraFrame.setSize(1920, 1080);
            cameraFrame.setBounds(0, 0, cameraFrame.getWidth(), cameraFrame.getHeight());
            VideoPanel videoPanel = new VideoPanel();
            cameraFrame.setContentPane(videoPanel);
            cameraFrame.setVisible(true);

            HOGDescriptor hog=new HOGDescriptor();
            // 获取模型文件
            hog.setSVMDetector(HOGDescriptor.getDefaultPeopleDetector());
            video(videoPanel, cameraFrame, hog);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void video (VideoPanel videoPanel, JFrame cameraFrame, HOGDescriptor faceCascade) throws Exception {
        // 调用摄像头
        VideoCapture capture = new VideoCapture();
        try {
//            capture.open(0);
            boolean isOpen = capture.open(url);

            System.out.println(isOpen);

//            MyWebSocketClient.connectSocket("ws://192.168.0.138:8880/", "192.168.0.48");
//            MoveThread moveThread = new MoveThread(0, 0, 0, 0);
//            moveThread.start();
            if (capture.isOpened()) {
                System.out.println("打开摄像头完成");
                Mat image = new Mat();
                while(true) {
                    capture.read(image);
                    if (!image.empty()) {
                        detectAndDisplay(image, faceCascade);
                        videoPanel.setImageWithMat(image);
                        cameraFrame.repaint();
                    } else {
                        break;
                    }
                }
            }
        } finally {
            capture.release();
        }
    }

    public static void detectAndDisplay(Mat frame, HOGDescriptor faceCascade)
    {
        MatOfRect rect = new MatOfRect();
        Mat gray = new Mat();

        Imgproc.cvtColor(frame, gray, Imgproc.COLOR_BGR2GRAY);

        Mat graySmall = new Mat();
        Imgproc.resize(gray, graySmall, new Size(gray.cols() / 3, gray.rows() / 3));

        // detect faces
        faceCascade.detectMultiScale(graySmall, rect, new MatOfDouble(), 0 , new Size(8,8), new Size(0,0), 1.05, 2, false);

        // each rectangle in faces is a face: draw them!
        Rect[] facesArray = rect.toArray();
        Integer x1 = 0;
        Integer y1 = 0;
        Integer x2 = 0;
        Integer y2 = 0;
        Integer x3 = 1920 / 2;
        Integer y3 = 1080 / 2;
        Integer imageX = 0;
        Integer imageY = 0;
        for (int i = 0; i < facesArray.length; i++){
            x1 = facesArray[i].x * 3 - 2;
            y1 = facesArray[i].y * 3 - 2;
            x2 = facesArray[i].x * 3 + facesArray[i].width * 3 - 5;
            y2 = facesArray[i].y * 3 + facesArray[i].height * 3;
//            System.out.println((facesArray[i].x * 3 - 2) + "   " + (facesArray[i].y * 3 - 2) + "   " + (facesArray[i].x * 3 + facesArray[i].width * 3 - 5) + "   " + (facesArray[i].y * 3 + facesArray[i].height * 3));
            imageX = x2 - x1;
            imageY = y2 - y1;
//            moveThread.setValue(x3, y3, imageX, imageY);
            Imgproc.rectangle(frame, new Point(facesArray[i].x * 3 - 2, facesArray[i].y * 3 - 2),
                   new Point(facesArray[i].x * 3 + facesArray[i].width * 3 - 5, facesArray[i].y * 3 + facesArray[i].height * 3), new Scalar(0, 255, 0), 3);
//                Imgproc.rectangle(frame, facesArray[i].tl(), facesArray[i].br(), new Scalar(0, 255, 0), 3);
        }
    }

}
