package com.oraclechain.test;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/9/15
 */
public class ZuoBiao {
    private Double x;
    private Double y;

    public ZuoBiao() {
    }

    public ZuoBiao(Double x, Double y) {
        this.x = x;
        this.y = y;
    }

    public Double getX() {
        return x;
    }

    public void setX(Double x) {
        this.x = x;
    }

    public Double getY() {
        return y;
    }

    public void setY(Double y) {
        this.y = y;
    }
}
