package com.oraclechain.entity;

import java.util.List;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/9/8
 */
public class Beacon {
    private String type;                // 类型
    private Integer user;               // 用户标识
    private Long time;                  // 信息发送时间
    private Integer helmet_battery;     // 安全帽电量
    private List<BeaconData> data;      // 数据
    private String coordinate;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getUser() {
        return user;
    }

    public void setUser(Integer user) {
        this.user = user;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public Integer getHelmet_battery() {
        return helmet_battery;
    }

    public void setHelmet_battery(Integer helmet_battery) {
        this.helmet_battery = helmet_battery;
    }

    public List<BeaconData> getData() {
        return data;
    }

    public void setData(List<BeaconData> data) {
        this.data = data;
    }

    public String getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(String coordinate) {
        this.coordinate = coordinate;
    }
}
