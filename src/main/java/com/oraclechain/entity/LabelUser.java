package com.oraclechain.entity;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/9/9
 */
public class LabelUser {
    private String labelId;
    private String name;
    private Boolean staff;
    private String accompanyName;
    private String acceptUserName;

    public LabelUser(String labelId, String name, Boolean staff, String accompanyName, String acceptUserName) {
        this.labelId = labelId;
        this.name = name;
        this.staff = staff;
        this.accompanyName = accompanyName;
        this.acceptUserName = acceptUserName;
    }

    public String getLabelId() {
        return labelId;
    }

    public void setLabelId(String labelId) {
        this.labelId = labelId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getStaff() {
        return staff;
    }

    public void setStaff(Boolean staff) {
        this.staff = staff;
    }

    public String getAccompanyName() {
        return accompanyName;
    }

    public void setAccompanyName(String accompanyName) {
        this.accompanyName = accompanyName;
    }

    public String getAcceptUserName() {
        return acceptUserName;
    }

    public void setAcceptUserName(String acceptUserName) {
        this.acceptUserName = acceptUserName;
    }
}
