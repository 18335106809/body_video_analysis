package com.oraclechain.entity;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/9/8
 */
public class BeaconData implements Comparable<BeaconData>{
    private Integer major;      // 信标组号
    private Integer minor;      // 信标标识
    private Integer battery;    // 电量(1---100)
    private Integer rssi;       // 信号强度(值为负数，越接近0信号越强，距离越近)

    public Integer getMajor() {
        return major;
    }

    public void setMajor(Integer major) {
        this.major = major;
    }

    public Integer getMinor() {
        return minor;
    }

    public void setMinor(Integer minor) {
        this.minor = minor;
    }

    public Integer getBattery() {
        return battery;
    }

    public void setBattery(Integer battery) {
        this.battery = battery;
    }

    public Integer getRssi() {
        return rssi;
    }

    public void setRssi(Integer rssi) {
        this.rssi = rssi;
    }

    @Override
    public int compareTo(BeaconData o) {
        return o.getRssi() - this.getRssi();
    }
}
