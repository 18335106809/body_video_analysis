package com.oraclechain.entity;

import java.util.Date;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/9/9
 */
public class ExternalInfo {

    private Long id;
    private Long accompanyingPersonId;
    private String accompanyingPersonName;
    private String cardNumber;
    private String cardType;
    private Date createTime;
    private String groupApprovalName;
    private Date groupApprovalTime;
    private Long lableId;
    private String lableNumber;
    private String leaderApprovalName;
    private Date leaderApprovalTime;
    private String location;
    private String name;
    private String phone;
    private Date scheduleEndTime;
    private Integer status;
    private Date updateTime;
    private Date visitEndTime;
    private Date visitStartTime;
    private String detail;
    private String peopleNum;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAccompanyingPersonId() {
        return accompanyingPersonId;
    }

    public void setAccompanyingPersonId(Long accompanyingPersonId) {
        this.accompanyingPersonId = accompanyingPersonId;
    }

    public String getAccompanyingPersonName() {
        return accompanyingPersonName;
    }

    public void setAccompanyingPersonName(String accompanyingPersonName) {
        this.accompanyingPersonName = accompanyingPersonName;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getGroupApprovalName() {
        return groupApprovalName;
    }

    public void setGroupApprovalName(String groupApprovalName) {
        this.groupApprovalName = groupApprovalName;
    }

    public Date getGroupApprovalTime() {
        return groupApprovalTime;
    }

    public void setGroupApprovalTime(Date groupApprovalTime) {
        this.groupApprovalTime = groupApprovalTime;
    }

    public Long getLableId() {
        return lableId;
    }

    public void setLableId(Long lableId) {
        this.lableId = lableId;
    }

    public String getLableNumber() {
        return lableNumber;
    }

    public void setLableNumber(String lableNumber) {
        this.lableNumber = lableNumber;
    }

    public String getLeaderApprovalName() {
        return leaderApprovalName;
    }

    public void setLeaderApprovalName(String leaderApprovalName) {
        this.leaderApprovalName = leaderApprovalName;
    }

    public Date getLeaderApprovalTime() {
        return leaderApprovalTime;
    }

    public void setLeaderApprovalTime(Date leaderApprovalTime) {
        this.leaderApprovalTime = leaderApprovalTime;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getScheduleEndTime() {
        return scheduleEndTime;
    }

    public void setScheduleEndTime(Date scheduleEndTime) {
        this.scheduleEndTime = scheduleEndTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getVisitEndTime() {
        return visitEndTime;
    }

    public void setVisitEndTime(Date visitEndTime) {
        this.visitEndTime = visitEndTime;
    }

    public Date getVisitStartTime() {
        return visitStartTime;
    }

    public void setVisitStartTime(Date visitStartTime) {
        this.visitStartTime = visitStartTime;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getPeopleNum() {
        return peopleNum;
    }

    public void setPeopleNum(String peopleNum) {
        this.peopleNum = peopleNum;
    }
}
