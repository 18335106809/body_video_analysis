package com.oraclechain.entity;

import java.util.Date;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/9/9
 */
public class StaffInfo {
    private Date acceptTime;
    private Long acceptUserId;
    private String acceptUserName;
    private Date actualDepartureTime;
    private Date createTime;
    private String description;
    private Long id;
    private Long lableId;
    private String lableNumber;
    private String location;
    private String pipeUserName;
    private Date ruleDepartureTime;
    private Integer status;
    private String submitterName;
    private Long submitterUserId;
    private Date updateTime;
    private String detail;

    public Date getAcceptTime() {
        return acceptTime;
    }

    public void setAcceptTime(Date acceptTime) {
        this.acceptTime = acceptTime;
    }

    public Long getAcceptUserId() {
        return acceptUserId;
    }

    public void setAcceptUserId(Long acceptUserId) {
        this.acceptUserId = acceptUserId;
    }

    public String getAcceptUserName() {
        return acceptUserName;
    }

    public void setAcceptUserName(String acceptUserName) {
        this.acceptUserName = acceptUserName;
    }

    public Date getActualDepartureTime() {
        return actualDepartureTime;
    }

    public void setActualDepartureTime(Date actualDepartureTime) {
        this.actualDepartureTime = actualDepartureTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getLableId() {
        return lableId;
    }

    public void setLableId(Long lableId) {
        this.lableId = lableId;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getPipeUserName() {
        return pipeUserName;
    }

    public void setPipeUserName(String pipeUserName) {
        this.pipeUserName = pipeUserName;
    }

    public Date getRuleDepartureTime() {
        return ruleDepartureTime;
    }

    public void setRuleDepartureTime(Date ruleDepartureTime) {
        this.ruleDepartureTime = ruleDepartureTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getSubmitterName() {
        return submitterName;
    }

    public void setSubmitterName(String submitterName) {
        this.submitterName = submitterName;
    }

    public Long getSubmitterUserId() {
        return submitterUserId;
    }

    public void setSubmitterUserId(Long submitterUserId) {
        this.submitterUserId = submitterUserId;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getLableNumber() {
        return lableNumber;
    }

    public void setLableNumber(String lableNumber) {
        this.lableNumber = lableNumber;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}
