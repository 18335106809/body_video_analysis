package com.oraclechain.entity;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/9/10
 */
public class PositionInfo {
    private String name;
    private String coordinate;
    private Boolean staff;
    private String accompanyName;
    private String acceptUserName;
    private Integer labelId;

    public PositionInfo() {
    }

    public PositionInfo(String name, String coordinate, Boolean staff, String accompanyName, String acceptUserName, Integer labelId) {
        this.name = name;
        this.coordinate = coordinate;
        this.staff = staff;
        this.accompanyName = accompanyName;
        this.acceptUserName = acceptUserName;
        this.labelId = labelId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(String coordinate) {
        this.coordinate = coordinate;
    }

    public String getAccompanyName() {
        return accompanyName;
    }

    public void setAccompanyName(String accompanyName) {
        this.accompanyName = accompanyName;
    }

    public String getAcceptUserName() {
        return acceptUserName;
    }

    public void setAcceptUserName(String acceptUserName) {
        this.acceptUserName = acceptUserName;
    }

    public Boolean getStaff() {
        return staff;
    }

    public void setStaff(Boolean staff) {
        this.staff = staff;
    }

    public Integer getLabelId() {
        return labelId;
    }

    public void setLabelId(Integer labelId) {
        this.labelId = labelId;
    }
}
