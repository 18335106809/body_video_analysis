package com.oraclechain.entity;

import java.util.Date;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/9/9
 */
public class LableManage {
    private Long id;
    private Date createTime;
    private Date updateTime;
    private String number;
    private String name;
    private String type;
    private String lendState;
    private String lableState;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLendState() {
        return lendState;
    }

    public void setLendState(String lendState) {
        this.lendState = lendState;
    }

    public String getLableState() {
        return lableState;
    }

    public void setLableState(String lableState) {
        this.lableState = lableState;
    }

    @Override
    public String toString() {
        return "LableManage{" +
                "id=" + id +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                ", number='" + number + '\'' +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", lendState='" + lendState + '\'' +
                ", lableState='" + lableState + '\'' +
                '}';
    }
}
