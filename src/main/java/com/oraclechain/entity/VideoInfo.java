package com.oraclechain.entity;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/10/20
 */
public class VideoInfo implements Comparable<VideoInfo> {

    private String name;

    private String ipAddress;

    private Long capsule;

    private String rtspAddress;

    /**
     * 0不需要记录 1需要记录
     */
    private Integer isRecord;

    /**
     *  0 未开始记录 1正在记录
     */
    private Boolean status;

    private String username;

    private String password;

    private String type;

    private String protocol;

    private String parameters;

    private String capsueLocation;

    private String position;

    private String iconfont;

    private String color;

    private String code;

    private String capsuleName;

    private Long spaceId;

    private String spaceName;

    private Integer capsuleNumber;

    private Integer xAxis;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public Long getCapsule() {
        return capsule;
    }

    public void setCapsule(Long capsule) {
        this.capsule = capsule;
    }

    public String getRtspAddress() {
        return rtspAddress;
    }

    public void setRtspAddress(String rtspAddress) {
        this.rtspAddress = rtspAddress;
    }

    public Integer getIsRecord() {
        return isRecord;
    }

    public void setIsRecord(Integer isRecord) {
        this.isRecord = isRecord;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getParameters() {
        return parameters;
    }

    public void setParameters(String parameters) {
        this.parameters = parameters;
    }

    public String getCapsueLocation() {
        return capsueLocation;
    }

    public void setCapsueLocation(String capsueLocation) {
        this.capsueLocation = capsueLocation;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getIconfont() {
        return iconfont;
    }

    public void setIconfont(String iconfont) {
        this.iconfont = iconfont;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCapsuleName() {
        return capsuleName;
    }

    public void setCapsuleName(String capsuleName) {
        this.capsuleName = capsuleName;
    }

    public Long getSpaceId() {
        return spaceId;
    }

    public void setSpaceId(Long spaceId) {
        this.spaceId = spaceId;
    }

    public String getSpaceName() {
        return spaceName;
    }

    public void setSpaceName(String spaceName) {
        this.spaceName = spaceName;
    }

    public Integer getCapsuleNumber() {
        return capsuleNumber;
    }

    public void setCapsuleNumber(Integer capsuleNumber) {
        this.capsuleNumber = capsuleNumber;
    }

    public Integer getxAxis() {
        return xAxis;
    }

    public void setxAxis(Integer xAxis) {
        this.xAxis = xAxis;
    }

    @Override
    public int compareTo(VideoInfo videoInfo) {
        return this.xAxis - videoInfo.xAxis;
    }
}
