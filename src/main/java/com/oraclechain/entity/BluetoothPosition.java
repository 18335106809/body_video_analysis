package com.oraclechain.entity;

import java.util.List;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/9/8
 */
public class BluetoothPosition {
    private Integer id;
    private Integer major;
    private Integer minor;
    private Long capsuleId;
    private Double longitude;
    private Double latitude;
    private String coordinate;

    public BluetoothPosition() {
    }

    public BluetoothPosition(Integer major, Integer minor) {
        this.major = major;
        this.minor = minor;
    }

    public BluetoothPosition(Integer id, Integer major, Integer minor, Long capsuleId, String coordinate) {
        this.id = id;
        this.major = major;
        this.minor = minor;
        this.capsuleId = capsuleId;
        this.coordinate = coordinate;
    }

    public Long getCapsuleId() {
        return capsuleId;
    }

    public void setCapsuleId(Long capsuleId) {
        this.capsuleId = capsuleId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMajor() {
        return major;
    }

    public void setMajor(Integer major) {
        this.major = major;
    }

    public Integer getMinor() {
        return minor;
    }

    public void setMinor(Integer minor) {
        this.minor = minor;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public String getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(String coordinate) {
        this.coordinate = coordinate;
    }
}
