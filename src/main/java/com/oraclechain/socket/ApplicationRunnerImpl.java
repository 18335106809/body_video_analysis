//package com.oraclechain.socket;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.ApplicationArguments;
//import org.springframework.boot.ApplicationRunner;
//import org.springframework.stereotype.Component;
//
///**
// * @author liuhualong
// * @Description:
// * @date 2020/9/9
// */
//@Component
//public class ApplicationRunnerImpl implements ApplicationRunner {
//
//    static Logger logger = LoggerFactory.getLogger(ApplicationRunnerImpl.class);
//
////    @Autowired
////    private PositionSocket positionSocket;
//
//    @Autowired
//    private SocketServerTest socketServerTest;
//
//    @Override
//    public void run(ApplicationArguments args) throws Exception {
//        logger.info("启动Socket服务...");
//        socketServerTest.start();
//    }
//}
