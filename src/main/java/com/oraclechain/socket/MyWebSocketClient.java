package com.oraclechain.socket;

import com.alibaba.fastjson.JSONObject;
import org.java_websocket.WebSocket;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/8/21
 */
//@Service("myWebSocketClient")
public class MyWebSocketClient extends WebSocketClient {

    static Logger logger = LoggerFactory.getLogger(MyWebSocketClient.class);

    private static Map<String, MyWebSocketClient> clientMap = new ConcurrentHashMap<>();

    private final static String STARTDISCOVERY = "startDiscovery";
    private final static String CONNECT = "connect";
    private final static String PTZMOVE = "ptzMove";
    private final static String PTZSTOP = "ptzStop";

    public MyWebSocketClient(String url) throws URISyntaxException {
        super(new URI(url));
    }

    @Override
    public void onOpen(ServerHandshake shake) {
        logger.info("进行连接握手...");
        for(Iterator<String> it = shake.iterateHttpFields(); it.hasNext();) {
            String key = it.next();
            System.out.println(key+":"+shake.getFieldValue(key));
        }
    }

    @Override
    public void onMessage(String paramString) {
        JSONObject result = JSONObject.parseObject(paramString);
        if (result.containsKey("id") && result.get("id").equals("connect")) {
            try {
//                sendMove();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        logger.info("接收到消息：" + paramString);
    }

    @Override
    public void onClose(int paramInt, String paramString, boolean paramBoolean) {
        logger.info("Socket连接关闭");
    }

    @Override
    public void onError(Exception e) {
        logger.error("Socket连接异常");
    }

    public void sendMove() throws Exception {
        JSONObject jsonMethod = new JSONObject();
        jsonMethod.put("method", "ptzMove");
        JSONObject jsonParam = new JSONObject();
        jsonParam.clear();
        jsonParam.put("address", "192.168.0.48");
        jsonParam.put("timeout", 30);
        JSONObject speed = new JSONObject();
        speed.put("x", 1);
        speed.put("y", 0);
        speed.put("z", 0);
        jsonParam.put("speed", speed);
        jsonMethod.put("params", jsonParam);
        System.out.println(jsonMethod.toJSONString());
        MyWebSocketClient client = clientMap.get("client");
        client.send(jsonMethod.toJSONString());

        Thread.currentThread().sleep(5000l);
        jsonMethod.clear();
        jsonParam.clear();
        jsonParam.put("address", "192.168.0.48");
        jsonMethod.put("method", "ptzStop");
        jsonMethod.put("params", jsonParam);
        client.send(jsonMethod.toJSONString());

    }

    public static void connectSocket(String url, String address) {
        try {
            MyWebSocketClient client = new MyWebSocketClient(url);
            client.connect();
            while (!client.getReadyState().equals(WebSocket.READYSTATE.OPEN)) {
                System.out.println("还没有打开");
            }
            clientMap.put(address, client);
            // 进行STARTDISCOVERY
            client.send(encodeJSON(STARTDISCOVERY, address, null, null, null, null, null).toJSONString());
            // 进行连接摄像头
            client.send(encodeJSON(CONNECT, address, "admin", "HikNZRSOK", null, null, null).toJSONString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void ptzMove(String address, Integer timeout, Float x, Float y) {
        if (!clientMap.containsKey(address)) {
            return;
        }
        try {
            MyWebSocketClient client = clientMap.get(address);
            client.send(encodeJSON(PTZMOVE, address, null, null, timeout, x, y).toJSONString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void ptzStop(String address) {
        if (!clientMap.containsKey(address)) {
            return;
        }
        try {
            MyWebSocketClient client = clientMap.get(address);
            client.send(encodeJSON(PTZSTOP, address, null, null, null, null, null).toJSONString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static JSONObject encodeJSON(String method, String address, String user, String pass, Integer timeout, Float x, Float y) {
        JSONObject result = new JSONObject();
        JSONObject params = new JSONObject();
        JSONObject speed = new JSONObject();
        if (!StringUtils.isEmpty(address)) {
            params.put("address", address);
        }
        if (!StringUtils.isEmpty(user)) {
            params.put("user", user);
        }
        if (!StringUtils.isEmpty(pass)) {
            params.put("pass", pass);
        }
        if (timeout != null) {
            params.put("timeout", timeout);
        }
        if (x != null) {
            speed.put("x", x);
        }
        if (y != null) {
            speed.put("y", y);
        }
        if (!CollectionUtils.isEmpty(speed)) {
            params.put("speed", speed);
        }
        result.put("method", method);
        result.put("params", params);
        System.out.println(result.toJSONString());
        return result;
    }

    public static void main(String[] args) {

        try {
            MyWebSocketClient client = new MyWebSocketClient("ws://192.168.0.138:8880/");
            client.connect();

            while (!client.getReadyState().equals(WebSocket.READYSTATE.OPEN)) {
                System.out.println("还没有打开");
            }
            System.out.println("建立websocket连接");
            clientMap.put("client", client);
            // 第一次
            JSONObject jsonMethod = new JSONObject();
            jsonMethod.put("method", "startDiscovery");
            JSONObject jsonParam = new JSONObject();
            jsonParam.put("address", "192.168.0.48");
            jsonMethod.put("params", jsonParam);
            client.send(jsonMethod.toJSONString());

            // 第二次
            jsonMethod.put("method", "connect");
            jsonParam.put("address", "192.168.0.48");
            jsonParam.put("user", "admin");
            jsonParam.put("pass", "HikNZRSOK");
            jsonMethod.put("params", jsonParam);
            client.send(jsonMethod.toJSONString());


            // 第三次
//            jsonMethod.put("method", "ptzMove");
//            jsonParam.clear();
//            jsonParam.put("address", "192.168.0.48");
//            jsonParam.put("timeout", 30);
//            JSONObject speed = new JSONObject();
//            speed.put("x", -1);
//            speed.put("y", -1);
//            speed.put("z", 0);
//            jsonParam.put("speed", speed);
//            jsonMethod.put("params", jsonParam);
//            System.out.println(jsonMethod.toJSONString());
//            client.send(jsonMethod.toJSONString());


            // 第四次 stop
//            jsonMethod.clear();
//            jsonMethod.put("method", "ptzStop");
//            client.send(jsonMethod.toJSONString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
