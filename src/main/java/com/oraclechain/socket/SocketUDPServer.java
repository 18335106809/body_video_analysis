package com.oraclechain.socket;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/9/13
 */
public class SocketUDPServer extends Thread {
    DatagramSocket server = null;
    public SocketUDPServer(int port) {
        try {
            server = new DatagramSocket(port);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void run(){

        super.run();
        try{
            System.out.println("wait client connect...");

            byte[] data = new byte[1024];// 创建字节数组，指定接收的数据包的大小
            DatagramPacket packet = new DatagramPacket(data, data.length);
            System.out.println("****服务器端已经启动，等待客户端发送数据");
            server.receive(packet);// 此方法在接收到数据报之前会一直阻塞

            String info = new String(data, 0, packet.getLength());
            System.out.println("我是服务器，客户端说：" + info);

        }catch (IOException e){
            e.printStackTrace();
        }
    }

    //函数入口
    public static void main(String[] args) {
        SocketUDPServer server = new SocketUDPServer(8234);
        server.start();
    }
}
