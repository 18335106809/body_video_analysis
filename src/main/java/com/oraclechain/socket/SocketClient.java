package com.oraclechain.socket;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

/**
 * @author liuhualong
 * @Description:     此代码测试串口服务器的工作模式为TCP server
 * @date 2020/9/11
 */
public class SocketClient {
    public void go() {

        try {

            // 创建socket
            Socket socketClient = new Socket("127.0.0.1", 8081);

            Scanner scanner=null;
            OutputStream out = null;
            try{
                if(socketClient != null){
                    scanner = new Scanner(System.in);
                    out = socketClient.getOutputStream();
                    String in = "";
                    do {
                        in = scanner.next();
                        out.write(("server saying: "+in).getBytes());
                        out.flush();//清空缓存区的内容
                    }while (!in.equals("q"));
                    scanner.close();
                    try{
                        out.close();
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }
            }catch (IOException e) {
                e.printStackTrace();
            }

            System.out.println(socketClient.isConnected());
            socketClient.setKeepAlive(true);
            System.out.println(socketClient.getKeepAlive());
            // 向服务器发送消息
            PrintWriter ps = new PrintWriter(out);

            ps.write("message to send!");
            ps.write("another message!");
            ps.close();
            // 从服务器读取消息
            InputStreamReader inputStreamReader = new InputStreamReader(socketClient.getInputStream());
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            System.out.println(socketClient.isConnected());
            System.out.println(socketClient.getKeepAlive());
            String s = bufferedReader.readLine();


            System.out.println("Client receive msg: " + s);
            inputStreamReader.close();

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        SocketClient sc = new SocketClient();
        sc.go();
        String str = "{\"type\":\"ibeacon\",\"user\":1001,\"time\":1337317493833, “helmet_battery”: 80, \"data\" : [{\"major\":10107,\"minor\":10107,\"battery\":88,\"rssi\":-14}, {\"major\":10073,\"minor\":10073,\"battery\":88,\"rssi\":-59}]}";
    }
}
