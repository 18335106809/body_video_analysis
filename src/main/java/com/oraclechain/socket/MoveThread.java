package com.oraclechain.socket;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/8/21
 */
public class MoveThread extends Thread {

    private Integer x3;
    private Integer y3;
    private Integer imageX;
    private Integer imageY;
    private Boolean isConnect;

    public Integer getX3() {
        return x3;
    }

    public void setX3(Integer x3) {
        this.x3 = x3;
    }

    public Integer getY3() {
        return y3;
    }

    public void setY3(Integer y3) {
        this.y3 = y3;
    }

    public Integer getImageX() {
        return imageX;
    }

    public void setImageX(Integer imageX) {
        this.imageX = imageX;
    }

    public Integer getImageY() {
        return imageY;
    }

    public void setImageY(Integer imageY) {
        this.imageY = imageY;
    }

    public Boolean getConnect() {
        return isConnect;
    }

    public void setConnect(Boolean connect) {
        isConnect = connect;
    }

    public MoveThread(Integer x3, Integer y3, Integer imageX, Integer imageY) {
        this.x3 = x3;
        this.y3 = y3;
        this.imageX = imageX;
        this.imageY = imageY;
    }

    public void setValue(Integer x3, Integer y3, Integer imageX, Integer imageY) {
        this.x3 = x3;
        this.y3 = y3;
        this.imageX = imageX;
        this.imageY = imageY;
    }

    public void setZero() {
        this.x3 = 0;
        this.y3 = 0;
        this.imageX = 0;
        this.imageY = 0;
    }

    @Override
    public void run() {

        while (true) {
            try {
                if (imageX != 0 && imageY != 0) {
                    if (x3 - imageX > 150) {
                        MyWebSocketClient.ptzMove("192.168.0.48", 30, -0.7F, 0F);
                    } else if (x3 - imageX < -150) {
                        MyWebSocketClient.ptzMove("192.168.0.48", 30, 0.7F, 0F);
                    }
//                    Thread.currentThread().sleep(1000L);
//                    MyWebSocketClient.ptzStop("192.168.0.48");
//                    if (y3 - imageY > 100) {
//                        MyWebSocketClient.ptzMove("192.168.0.48", 30, 0F, -1F);
//                    } else if (y3 - imageY < -50) {
//                        MyWebSocketClient.ptzMove("192.168.0.48", 30, 0F, 1F);
//                    }
                    Thread.currentThread().sleep(1000L);
                    MyWebSocketClient.ptzStop("192.168.0.48");
                    setZero();
                }
                Thread.currentThread().sleep(5000L);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
