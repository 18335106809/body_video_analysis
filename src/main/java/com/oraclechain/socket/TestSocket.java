package com.oraclechain.socket;

import com.alibaba.fastjson.JSONObject;
import com.oraclechain.config.RedisKeyConfig;
import com.oraclechain.entity.Beacon;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;
import sun.applet.Main;

import java.net.InetSocketAddress;
import java.util.concurrent.TimeUnit;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/9/7
 */
public class TestSocket extends WebSocketServer{

    static Logger logger = LoggerFactory.getLogger(TestSocket.class);

    public TestSocket() {
        super(new InetSocketAddress(8234));
    }

    @Override
    public void onOpen(WebSocket webSocket, ClientHandshake clientHandshake) {
        logger.info("连上了");
        // webSocket建立连接的时候触发的代码
    }

    @Override
    public void onClose(WebSocket webSocket, int i, String s, boolean b) {
        logger.info("断开连接......原因：" + s);
    }

    @Override
    public void onMessage(WebSocket webSocket, String message) {
        logger.info("接收到消息......" + message);
        // TODO: 2020/9/8 接收到客户端消息后业务处理
    }

    @Override
    public void onError(WebSocket webSocket, Exception e) {
        logger.error("on error");
        e.printStackTrace();
    }

    @Override
    public void onStart() {

    }

    public static void main(String[] args) {
        TestSocket test = new TestSocket();
        test.start();
    }

}
