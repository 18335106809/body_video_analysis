//package com.oraclechain.socket;
//
//import com.alibaba.fastjson.JSONObject;
//import com.oraclechain.config.RedisKeyConfig;
//import com.oraclechain.entity.Beacon;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.data.redis.core.RedisTemplate;
//import org.springframework.stereotype.Service;
//
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.OutputStream;
//import java.net.ServerSocket;
//import java.net.Socket;
//import java.util.Scanner;
//import java.util.concurrent.TimeUnit;
//
///**
// * @author liuhualong
// * @Description: 此类测试串口服务的工作模式为TCP client
// * @date 2020/9/13
// */
//@Service("socketServerTest")
//public class SocketServerTest extends Thread {
//
//    static Logger logger = LoggerFactory.getLogger(SocketServerTest.class);
//
//    @Autowired
//    private RedisTemplate<String, String> redisTemplate;
//
//    @Value("${position.time.stay}")
//    private String stayTime;
//
//    ServerSocket server = null;
//    Socket socket = null;
//    public SocketServerTest() {
//        try {
//            server = new ServerSocket(8081);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//    @Override
//    public void run(){
//
//        super.run();
//        try{
//            logger.info("wait client connect...");
//            socket = server.accept();
////            new sendMessThread().start();//连接并返回socket后，再启用发送消息线程
//            logger.info(socket.getInetAddress().getHostAddress()+"SUCCESS TO CONNECT...");
//            InputStream in = socket.getInputStream();
//            int len = 0;
//            byte[] buf = new byte[4168];
//            while ((len=in.read(buf))!=-1){
//                logger.info("client saying: "+new String(buf,0, len));
//                String data = new String(buf,0, len);
//                data = data.replace("server saying: ", "");
//                logger.info("string data : " + data);
//                Beacon beaconJson = JSONObject.parseObject(data, Beacon.class);
//                redisTemplate.opsForValue().set(RedisKeyConfig.USER_POSITION_KEY + beaconJson.getUser(), data, Integer.parseInt(stayTime), TimeUnit.MINUTES);
//            }
//
//        }catch (IOException e){
//            e.printStackTrace();
//        }
//    }
//
//    //函数入口
//    public static void main(String[] args) {
//        SocketServerTest server = new SocketServerTest();
//        server.start();
//    }
//}
