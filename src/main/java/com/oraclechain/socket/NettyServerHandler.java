package com.oraclechain.socket;

import com.alibaba.fastjson.JSONObject;
import com.oraclechain.config.RedisKeyConfig;
import com.oraclechain.entity.Beacon;
import com.oraclechain.service.LightService;
import com.oraclechain.util.SpringContextUtil;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.concurrent.TimeUnit;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/9/26
 */
public class NettyServerHandler extends ChannelInboundHandlerAdapter {

    static Logger logger = LoggerFactory.getLogger(NettyServerHandler.class);

    private static RedisTemplate<String, String> redisTemplate;

    private static LightService lightService;

    static {
        redisTemplate = SpringContextUtil.getBean("stringRedisTemplate", RedisTemplate.class);
        lightService = SpringContextUtil.getBean(LightService.class);
    }

    /**
     * 客户端连接会触发
     */
    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        logger.info("Channel active......");
    }

    /**
     * 客户端发消息会触发
     */
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        String message = msg.toString();
        logger.info("receive message : {}", message);
        message = message.replace("server saying: ", "");
        Beacon beaconJson = JSONObject.parseObject(message, Beacon.class);
        if (!StringUtils.isEmpty(beaconJson.getType()) && beaconJson.getType().equals("ibeacon")) {
            redisTemplate.opsForValue().set(RedisKeyConfig.USER_POSITION_KEY + beaconJson.getUser(), message, 10, TimeUnit.MINUTES);
            lightService.openLight(beaconJson);
        }
    }

    /**
     * 发生异常触发
     */
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        cause.printStackTrace();
        ctx.close();
    }
}
