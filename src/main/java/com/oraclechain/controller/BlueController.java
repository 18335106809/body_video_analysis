package com.oraclechain.controller;

import com.oraclechain.entity.BluetoothPosition;
import com.oraclechain.service.BluetoothPositionService;
import com.oraclechain.test.ZuoBiao;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/9/16
 */
@RestController
@RequestMapping("/blue")
@Api(description = "人员定位相关操作")
public class BlueController {

    static Logger logger = LoggerFactory.getLogger(PositionController.class);

    @Autowired
    private BluetoothPositionService bluetoothPositionService;

    @RequestMapping(value = "insert", method = RequestMethod.POST)
    @ResponseBody
    public String getUSRtest(@RequestParam("num") Integer num, @RequestParam("line") Integer line) {
        File file = new File("C:\\Users\\Think\\Desktop\\ggg.txt");
        List<String> dataList = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(file));) {
            String lineData = null;
            while ((lineData = br.readLine()) != null) {
                System.out.println(dataList.add(lineData.trim()));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        List<ZuoBiao> firstHalf = new ArrayList<>();
        List<ZuoBiao> secondHalf = new ArrayList<>();
        for (int i = 1; i <= 23; i++) {
            String str = dataList.get(i - 1);
            str = str.substring(1, str.length() - 2);
            String[] array = str.split(",");
            firstHalf.add(new ZuoBiao(Double.parseDouble(array[0]), Double.parseDouble(array[1])));
        }
        for (int i = 46; i >= 24; i--) {
            String str = dataList.get(i - 1);
            str = str.substring(1, str.length() - 2);
            String[] array = str.split(",");
            secondHalf.add(new ZuoBiao(Double.parseDouble(array[0]), Double.parseDouble(array[1])));
        }
        ZuoBiao a = firstHalf.get(num - 1);
        ZuoBiao b = firstHalf.get(num - 1 + 1);
        ZuoBiao d = secondHalf.get(num - 1);
        ZuoBiao c = secondHalf.get(num - 1 + 1);
        List<ZuoBiao> zuoBiaoList = getZuoBiao(a, b, c, d, line);
        for (ZuoBiao zuoBiao : zuoBiaoList) {
            BluetoothPosition bluetoothPosition = new BluetoothPosition();
            bluetoothPosition.setCapsuleId(4000L + num);
            bluetoothPosition.setCoordinate("[" + zuoBiao.getX() + "," + zuoBiao.getY() + "]");
            bluetoothPositionService.insert(bluetoothPosition);
        }
        return null;
    }

    @RequestMapping(value = "insertClip", method = RequestMethod.POST)
    @ResponseBody
    public String getUSRtest(@RequestParam("capsuleId") Long capsuleId) {
        File file = new File("C:\\Users\\Think\\Desktop\\ggg.txt");
        List<String> dataList = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String lineData = null;
            while ((lineData = br.readLine()) != null) {
                dataList.add(lineData.trim());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        List<ZuoBiao> zuoBiaos = new ArrayList<>();
        for (int i = 1; i <= dataList.size(); i++) {
            String str = dataList.get(i - 1);
            str = str.substring(1, str.length() - 2);
            String[] array = str.split(",");
            zuoBiaos.add(new ZuoBiao(Double.parseDouble(array[0]), Double.parseDouble(array[1])));
        }
        ZuoBiao a = zuoBiaos.get(0);
        ZuoBiao b = zuoBiaos.get(1);
        ZuoBiao c = zuoBiaos.get(2);
        ZuoBiao d = zuoBiaos.get(3);
        ZuoBiao m = getZuoBiao(a, d);
        ZuoBiao n = getZuoBiao(b, c);
        ZuoBiao zuoBiao = getZuoBiao(a, b, n, m);
        BluetoothPosition bluetoothPosition = new BluetoothPosition();
        bluetoothPosition.setCapsuleId(1000L + capsuleId);
        bluetoothPosition.setCoordinate("[" + zuoBiao.getX() + "," + zuoBiao.getY() + "]");
        bluetoothPositionService.insert(bluetoothPosition);
        zuoBiao = getZuoBiao(m, n, c, d);
        bluetoothPosition.setCapsuleId(3000L + capsuleId);
        bluetoothPosition.setCoordinate("[" + zuoBiao.getX() + "," + zuoBiao.getY() + "]");
        bluetoothPositionService.insert(bluetoothPosition);
        return null;
    }

    public static List<ZuoBiao> getZuoBiao(ZuoBiao a, ZuoBiao b, ZuoBiao c, ZuoBiao d, Integer time) {
        List<ZuoBiao> result = new ArrayList<>();
        Double ab_x = (b.getX() - a.getX()) / (time + 1);
        Double ab_y = (b.getY() - a.getY()) / (time + 1);
        Double cd_x = (c.getX() - d.getX()) / (time + 1);
        Double cd_y = (c.getY() - d.getY()) / (time + 1);
        for (int i = 1; i <= time; i++) {
            ZuoBiao zuoBiao = new ZuoBiao();
            zuoBiao.setX((a.getX() + ab_x + d.getX() + cd_x) / 2);
            zuoBiao.setY((a.getY() + ab_y + d.getY() + cd_y) / 2);
            result.add(zuoBiao);
            a.setX(a.getX() + ab_x);
            a.setY(a.getY() + ab_y);
            d.setX(d.getX() + cd_x);
            d.setY(d.getY() + cd_y);
        }
        return result;
    }

    public static ZuoBiao getZuoBiao(ZuoBiao a, ZuoBiao b, ZuoBiao c, ZuoBiao d) {
        Double ab_x = (b.getX() + a.getX()) / 2;
        Double ab_y = (b.getY() + a.getY()) / 2;
        Double cd_x = (c.getX() + d.getX()) / 2;
        Double cd_y = (c.getY() + d.getY()) / 2;
        ZuoBiao zuoBiao = new ZuoBiao();
        zuoBiao.setX((ab_x + cd_x) / 2);
        zuoBiao.setY((ab_y + cd_y) / 2);
        return zuoBiao;
    }

    public static ZuoBiao getZuoBiao(ZuoBiao p, ZuoBiao q) {
        ZuoBiao zuoBiao = new ZuoBiao();
        zuoBiao.setX((p.getX() + q.getX()) / 2);
        zuoBiao.setY((p.getY() + q.getY()) / 2);
        return zuoBiao;
    }
}
