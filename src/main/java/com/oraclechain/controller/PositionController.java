package com.oraclechain.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.oraclechain.config.RedisKeyConfig;
import com.oraclechain.service.BluetoothPositionService;
import com.oraclechain.util.ParameterUtil;
import com.oraclechain.util.ResultUtil;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpRequest;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/9/8
 */
@RestController
@RequestMapping("/position")
@Api(description = "人员定位相关操作")
public class PositionController {

    static Logger logger = LoggerFactory.getLogger(PositionController.class);

    @Autowired
    private BluetoothPositionService bluetoothPositionService;

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @RequestMapping(value = "getPositionInfo", method = RequestMethod.POST)
    @ResponseBody
    public String getPositionInfo() {
        logger.info("get user position info......");
        try {
            Object result = bluetoothPositionService.getPositionInfo();
            return JSONObject.toJSONString(ResultUtil.success(result));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("get user position info have error");
            return JSONObject.toJSONString(ResultUtil.error("查询数据失败"));
        }
    }

    @RequestMapping(value = "insertLocation", method = RequestMethod.POST)
    @ResponseBody
    public String insertLocation(@RequestBody Map<String, Object> map) {
        logger.info("insert location info......");
        try {
            redisTemplate.opsForValue().set(RedisKeyConfig.POSITION_LOCATION_KEY, JSONObject.toJSONString(map));
            return JSONObject.toJSONString(ResultUtil.success());
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("insert location info have error");
            return JSONObject.toJSONString(ResultUtil.error("查询数据失败"));
        }
    }

    @RequestMapping(value = "getLocation", method = RequestMethod.GET)
    @ResponseBody
    public String getLocation() {
        logger.info("get location info......");
        try {
            String value = redisTemplate.opsForValue().get(RedisKeyConfig.POSITION_LOCATION_KEY);
            JSONObject jsonObject = JSONObject.parseObject(value);
            return JSONObject.toJSONString(ResultUtil.success(jsonObject));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("get location info have error");
            return JSONObject.toJSONString(ResultUtil.error("查询数据失败"));
        }
    }

    @RequestMapping(value = "getVideoInfo", method = RequestMethod.POST)
    @ResponseBody
    public String getVideoInfo(@RequestBody Map<String, Object> map) {
        logger.info("get video info linked user position");
        Long capsuleId = ParameterUtil.getParameter(map, "capsuleId", Long.class);
        String coordinate = ParameterUtil.getParameter(map, "coordinate", String.class);
        try {
            Object result = bluetoothPositionService.getVideoInfo(capsuleId, coordinate);
            if (result == null) {
                return JSONObject.toJSONString(ResultUtil.error("获取摄像头数据失败"));
            } else {
                return JSONObject.toJSONString(ResultUtil.success(result));
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("get video info linked have error");
            return JSONObject.toJSONString(ResultUtil.error("获取摄像头数据失败"));
        }
    }

    @RequestMapping(value = "getUSRtest", method = RequestMethod.GET)
    @ResponseBody
    public String getUSRtest(@RequestParam("data") String data) {
        logger.info("get user position info......" + data);
        return null;
    }

}
