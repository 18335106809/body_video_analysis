package com.oraclechain;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.openfeign.EnableFeignClients;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/8/19
 */
@SpringBootApplication
@EnableFeignClients
@EnableDiscoveryClient
@EnableSwagger2
@RefreshScope
public class WalkFindApplication {
    public static void main(String[] args) {
        SpringApplication.run(WalkFindApplication.class);
    }
}
