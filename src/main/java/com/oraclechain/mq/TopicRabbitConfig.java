package com.oraclechain.mq;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/1/6
 */
@Configuration
public class TopicRabbitConfig {

    public final static String EXCHANGE = "topicExchange";      // 消费消息交换机

    public final static String EXCHANGE_OUT = "exchange_out";   // 生产消息交换机

    //绑定topic
    public final static String DEVICE = "device";               // 接收消息路由

    //下发硬件指令 topic
    public final static String DEVICE_OPEN = "device_open";   // 生产消息路由

    //下发硬件指令 topic
    public final static String DEVICE_CLOSE = "device_close";   // 生产消息路由

    @Bean
    public Queue deviceQueue() {
        return new Queue(DEVICE);
    }

    @Bean
    public Queue deviceOpenQueue() {
        return new Queue(DEVICE_OPEN);
    }

    @Bean
    public Queue deviceCloseQueue() {
        return new Queue(DEVICE_CLOSE);
    }

    @Bean
    TopicExchange exchange() {
        return new TopicExchange(EXCHANGE);
    }

    @Bean
    TopicExchange exchangeOut() {
        return new TopicExchange(EXCHANGE_OUT);
    }

    @Bean
    Binding bindingExchangeMessage() {
        return BindingBuilder.bind(deviceQueue()).to(exchange()).with(DEVICE);
    }

    @Bean
    Binding bindingExchangeOutOpenMessage() {
        return BindingBuilder.bind(deviceOpenQueue()).to(exchangeOut()).with(DEVICE_OPEN);
    }

    @Bean
    Binding bindingExchangeOutCloseMessage() {
        return BindingBuilder.bind(deviceCloseQueue()).to(exchangeOut()).with(DEVICE_CLOSE);
    }


}
