package com.oraclechain.service;

import com.oraclechain.entity.BluetoothPosition;

import java.util.List;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/9/8
 */
public interface BluetoothPositionService {
    void insert(BluetoothPosition bluetoothPosition);
    void update(BluetoothPosition bluetoothPosition);
    void delete(Integer id);
    List<BluetoothPosition> getData(BluetoothPosition bluetoothPosition);
    Object getPositionInfo();
    Object getVideoInfo(Long capsuleId, String coordinate);
}
