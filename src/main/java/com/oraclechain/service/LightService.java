package com.oraclechain.service;

import com.oraclechain.entity.Beacon;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/9/21
 */
public interface LightService {
    void openLight(Beacon beacon);
    void closeLight();
}
