package com.oraclechain.service;

import com.oraclechain.entity.Device;
import com.oraclechain.entity.LabelUser;
import com.oraclechain.entity.VideoInfo;

import java.util.List;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/9/9
 */
public interface FeignService {
    List<LabelUser> getExternalInfo();
    List<LabelUser> getStaffInfo();
    List<Device> getDevice(String type, Long capsuleId);
    List<VideoInfo> getVideo(Long capsuleId);
}
