package com.oraclechain.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.oraclechain.config.RedisKeyConfig;
import com.oraclechain.dao.BluetoothPositionDao;
import com.oraclechain.entity.*;
import com.oraclechain.service.BluetoothPositionService;
import com.oraclechain.service.FeignService;
import com.oraclechain.util.ParameterUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/9/8
 */
@Service
public class BluetoothPositionServiceImpl implements BluetoothPositionService {

    static Logger logger = LoggerFactory.getLogger(BluetoothPositionServiceImpl.class);

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Autowired
    private BluetoothPositionDao bluetoothPositionDao;

    @Autowired
    private FeignService feignService;

    @Override
    public void insert(BluetoothPosition bluetoothPosition) {
        bluetoothPositionDao.insert(bluetoothPosition);
    }

    @Override
    public void update(BluetoothPosition bluetoothPosition) {
        bluetoothPositionDao.update(bluetoothPosition);
    }

    @Override
    public void delete(Integer id) {
        bluetoothPositionDao.delete(id);
    }

    @Override
    public List<BluetoothPosition> getData(BluetoothPosition bluetoothPosition) {
        return bluetoothPositionDao.getData(bluetoothPosition);
    }

    @Override
    public Object getPositionInfo() {
        Map<Long, List<Beacon>> beaconUserMap = new ConcurrentHashMap<>();
        Set<String> lableUsedSet = new HashSet<>();
        Set<String> keys = redisTemplate.keys(RedisKeyConfig.USER_POSITION_KEY + "*");
        logger.info("get position info from redis, keys:" + keys.size());
        for (String key : keys) {
            String beaconJson = redisTemplate.opsForValue().get(key);
            Beacon beacon = JSONObject.parseObject(beaconJson, Beacon.class);
            if (beacon == null || beacon.getUser() == null) {
                continue;
            }
            lableUsedSet.add(beacon.getUser().toString());
            List<BeaconData> beaconDataList = beacon.getData();
            if (CollectionUtils.isEmpty(beaconDataList)) {
                continue;
            }
            Collections.sort(beaconDataList);
            BeaconData beaconData = beaconDataList.get(0);
            List<BluetoothPosition> bluetoothPositions = getData(new BluetoothPosition(null, beaconData.getMajor(), beaconData.getMinor(), null, null));
            if (CollectionUtils.isEmpty(bluetoothPositions)) {
                continue;
            }
            BluetoothPosition bluetoothPosition = bluetoothPositions.get(0);
            Long capsuleId = bluetoothPosition.getCapsuleId();
            beacon.setCoordinate(bluetoothPosition.getCoordinate());
            if (capsuleId != null) {
                List<Beacon> list = new ArrayList<>();
                if (beaconUserMap.containsKey(capsuleId)) {
                    list = beaconUserMap.get(capsuleId);
                }
                list.add(beacon);
                beaconUserMap.put(capsuleId, list);
            }
        }
        return dataCollation(beaconUserMap, lableUsedSet);
    }

    public Object dataCollation(Map<Long, List<Beacon>> beaconUserMap, Set<String> lableUsedSet) {// 每个段下有哪些蓝牙信号，蓝牙信号集合
        logger.info("have position label collection is:" + lableUsedSet);
        List<LabelUser> externalInfoList = feignService.getExternalInfo();
        List<LabelUser> staffInfoList = feignService.getStaffInfo();
        logger.info("external info list:" + externalInfoList.size() + ", staff info list:" + staffInfoList.size());
        externalInfoList.addAll(staffInfoList);
        Map<Long, List<PositionInfo>> result = new HashMap<>();
        for (Map.Entry<Long, List<Beacon>> entry : beaconUserMap.entrySet()) {
            Long capsuleId = entry.getKey();
            List<Beacon> beaconUser = entry.getValue();
            Map<String, Beacon> lableIdMap = new HashMap<>();   // lableId : beacon
            for (Beacon beacon : beaconUser) {
                lableIdMap.put(beacon.getUser().toString(), beacon);
            }
            List<PositionInfo> userName = new ArrayList<>();
            Set<String> labelNameSet = new HashSet<>();
            for (LabelUser labelUser : externalInfoList) {
                if (!lableUsedSet.contains(labelUser.getLabelId())) {
                    continue;
                }
                // TODO: 2020/9/15
                if (lableIdMap.containsKey(labelUser.getLabelId())) {
                    Beacon beacon = lableIdMap.get(labelUser.getLabelId());
                    userName.add(new PositionInfo(labelUser.getName(), beacon.getCoordinate(), labelUser.getStaff(),
                            labelUser.getAccompanyName(), labelUser.getAcceptUserName(), Integer.valueOf(labelUser.getLabelId())));
                    labelNameSet.add(labelUser.getName());
                }
            }
            if (!CollectionUtils.isEmpty(userName)) {
                result.put(capsuleId, userName);
            }
        }
        logger.info("lable user in capsule : " + result);
        List<JSONObject> jsonObjectList = new ArrayList<>();
        Map<Long, JSONObject> spaceManageMap = new HashMap<>();
        // result 为每个防火段下的人，改成每个舱下的人
        Map<Long, List<SpaceManage>> spacePositionMap = new HashMap<>();
        for (Map.Entry<Long, List<PositionInfo>> entry : result.entrySet()) {
            Long capsuleId = entry.getKey();
            SpaceManage spaceManage = getSpaceById(capsuleId);
            List<SpaceManage> spaceManages = new ArrayList<>();
            spaceManage.setPositionData(entry.getValue());
            if (spaceManage != null) {
                Long spaceId = spaceManage.getParentId();
                if (spaceManageMap.containsKey(spaceId)) {
                    spaceManages = spacePositionMap.get(spaceId);
                }
                spaceManages.add(spaceManage);
                spacePositionMap.put(spaceId, spaceManages);
            }
        }
        for (Map.Entry<Long, List<SpaceManage>> entry : spacePositionMap.entrySet()) {
            JSONObject object = new JSONObject();
            object.put("spaceId", entry.getKey());
            object.put("capsuleData", entry.getValue());
            jsonObjectList.add(object);
        }
//        for (Map.Entry<Long, List<PositionInfo>> entry : result.entrySet()) {
//            JSONObject jsonObject = new JSONObject();
//            List<SpaceManage> spaceManages = new ArrayList<>();
//
//            Long capsuleId = entry.getKey();
//            List<PositionInfo> nameList = entry.getValue();
//            SpaceManage spaceManage = getSpaceById(capsuleId);
//            if (spaceManage != null) {
//                Long spaceId = spaceManage.getParentId();
//                if (spaceManageMap.containsKey(spaceId)) {
//                    jsonObject = spaceManageMap.get(spaceId);
//                    spaceManages = JSONArray.parseArray(jsonObject.get("capsuleData").toString(), SpaceManage.class);
//                }
//                spaceManage.setPositionData(nameList);
//                spaceManages.add(spaceManage);
//                jsonObject.put("spaceId", spaceId);
//                jsonObject.put("capsuleData", spaceManages);
//                spaceManageMap.put(spaceId, jsonObject);
//            }
//        }
//        for (Map.Entry<Long, JSONObject> entry : spaceManageMap.entrySet()) {
//            jsonObjectList.add(entry.getValue());
//        }
        return jsonObjectList;
    }

    public SpaceManage getSpaceById(Long id) {
        String data = redisTemplate.opsForValue().get(RedisKeyConfig.DEVICE_SPACE_KEY + id);
        return JSONObject.parseObject(data, SpaceManage.class);
    }

    @Override
    public Object getVideoInfo(Long capsuleId, String coordinate) {
        List<VideoInfo> videoInfoList = feignService.getVideo(capsuleId);
        if (CollectionUtils.isEmpty(videoInfoList)) {
            return null;
        }
        BluetoothPosition bluetoothPosition = new BluetoothPosition();
        bluetoothPosition.setCapsuleId(capsuleId);
        List<BluetoothPosition> positionList = getData(bluetoothPosition);
        if (CollectionUtils.isEmpty(positionList)) {
            return videoInfoList.get(0);
        }
        for (VideoInfo videoInfo : videoInfoList) {
            videoInfo.setxAxis(ParameterUtil.toArray(videoInfo.getPosition()));
        }
        Collections.sort(videoInfoList);
        Integer videoSize = videoInfoList.size();
        Integer positionSize = positionList.size();
        Integer multiple = positionSize / videoSize;
        Integer index = 0;
        for (int i = 1; i <= positionSize; i++) {
            BluetoothPosition position = positionList.get(i - 1);
            if (position.getCoordinate().equals(coordinate)) {
                index = i;
                break;
            }
        }
        Integer videoIndex = index / multiple;
        if (index % multiple != 0) {
            videoIndex += 1;
        }
        if (videoIndex > 0) {
            if (videoIndex > videoSize) {
                return videoInfoList.get(videoSize - 1);
            } else {
                return videoInfoList.get(videoIndex - 1);
            }
        } else {
            return videoInfoList.get(0);
        }
    }
}
