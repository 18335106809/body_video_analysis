package com.oraclechain.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.oraclechain.config.RedisKeyConfig;
import com.oraclechain.entity.Beacon;
import com.oraclechain.entity.BeaconData;
import com.oraclechain.entity.BluetoothPosition;
import com.oraclechain.entity.Device;
import com.oraclechain.mq.TopicRabbitConfig;
import com.oraclechain.service.BluetoothPositionService;
import com.oraclechain.service.FeignService;
import com.oraclechain.service.LightService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/9/21
 */
@Service
public class LightServiceImpl implements LightService {

    static Logger logger = LoggerFactory.getLogger(LightServiceImpl.class);

    @Autowired
    private BluetoothPositionService bluetoothPositionService;

    @Autowired
    private FeignService feignService;

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Value("${device.type.light}")
    private String light;

    @Value("${device.type.light.timeout}")
    private String timeout;

    /**
     * 人员定位和照明的联动
     * @param beacon
     */
    @Override
    @Async("asyncServiceExecutor")
    public void openLight(Beacon beacon) {
        try {
            List<BeaconData> beaconDataList = beacon.getData();
            if (CollectionUtils.isEmpty(beaconDataList)) {
                return;
            }
            Collections.sort(beaconDataList);
            BeaconData beaconData = beaconDataList.get(0);
            List<BluetoothPosition> bluetoothPositions = bluetoothPositionService.getData(new BluetoothPosition(beaconData.getMajor(), beaconData.getMinor()));
            if (CollectionUtils.isEmpty(bluetoothPositions)) {
                return;
            }
            BluetoothPosition bluetoothPosition = bluetoothPositions.get(0);
            Long capsuleId = bluetoothPosition.getCapsuleId();
            if (capsuleId != null) {
                // 判断此防火段是否已经开灯
                if (redisTemplate.hasKey(RedisKeyConfig.DEVICE_CAPSULE_LIGHT_KEY + capsuleId)) {
                    return;
                }
                // 通过防火段的ID，查找所有的照明设备
                List<Device> deviceList = feignService.getDevice(light, capsuleId);
                for (Device device : deviceList) {
                    // TODO: 2020/6/29 下发打开指令
                    rabbitTemplate.convertAndSend(TopicRabbitConfig.EXCHANGE_OUT, TopicRabbitConfig.DEVICE_OPEN, JSONObject.toJSONString(device));
                }
                redisTemplate.opsForValue().set(RedisKeyConfig.DEVICE_CAPSULE_LIGHT_KEY + capsuleId, capsuleId.toString(),
                        Integer.parseInt(timeout), TimeUnit.MINUTES);
                // 关闭灯的定时任务
                Thread.sleep(10 * 60 * 1000L);
                for (Device device : deviceList) {
                    rabbitTemplate.convertAndSend(TopicRabbitConfig.EXCHANGE_OUT, TopicRabbitConfig.DEVICE_OPEN, JSONObject.toJSONString(device));
                }
                redisTemplate.delete(RedisKeyConfig.DEVICE_CAPSULE_LIGHT_KEY + capsuleId);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void closeLight() {

    }

}
