package com.oraclechain.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.oraclechain.entity.*;
import com.oraclechain.feign.AssetsClient;
import com.oraclechain.feign.DeviceClient;
import com.oraclechain.feign.ProviderClient;
import com.oraclechain.service.FeignService;
import com.oraclechain.util.ParameterUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/9/9
 */
@Service
public class FeignServiceImpl implements FeignService {

    static Logger logger = LoggerFactory.getLogger(FeignServiceImpl.class);

    static SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Autowired
    private ProviderClient providerClient;

    @Autowired
    private AssetsClient assetsClient;

    @Autowired
    private DeviceClient deviceClient;

    @Value("${into.pipe.time.interval}")
    private String interval;

    @Override
    public List<LabelUser> getExternalInfo() {
        List<LabelUser> result = new ArrayList<>();
        try {
            JSONObject jsonObject = JSONObject.parseObject(providerClient.getExStaffInfo(setParams()));
            if (jsonObject != null && Integer.valueOf(jsonObject.get("code").toString()).equals(0) && jsonObject.containsKey("data")) {
                JSONObject dataJSON = JSONObject.parseObject(jsonObject.get("data").toString());
                List<ExternalInfo> externalInfos = JSONArray.parseArray(dataJSON.get("records").toString(), ExternalInfo.class);
                logger.info("external info list size : " + externalInfos.size());
                Set<Long> lableIdSet = new HashSet<>();
                for (ExternalInfo externalInfo : externalInfos) {
                    if (externalInfo.getLableId() == null || lableIdSet.contains(externalInfo.getLableId())) {
                        continue;
                    }
                    if (StringUtils.isEmpty(externalInfo.getLableNumber())) {
                        continue;
                    }
                    if (isOverTime(externalInfo.getUpdateTime())) {
                        logger.info("the external info is over time: " + externalInfo.getId());
                        continue;
                    }
                    lableIdSet.add(externalInfo.getLableId());
                    result.add(new LabelUser(externalInfo.getLableNumber(), externalInfo.getName(), false,
                            externalInfo.getAccompanyingPersonName(), externalInfo.getGroupApprovalName()));
                }
            }
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return result;
        }
    }

    @Override
    public List<LabelUser> getStaffInfo() {
        List<LabelUser> result = new ArrayList<>();
        try {
            JSONObject jsonObject = JSONObject.parseObject(providerClient.getStaffInfo(setParams()));
            if (jsonObject != null && Integer.valueOf(jsonObject.get("code").toString()).equals(0) && jsonObject.containsKey("data")) {
                JSONObject dataJSON = JSONObject.parseObject(jsonObject.get("data").toString());
                List<StaffInfo> staffInfos = JSONArray.parseArray(dataJSON.get("records").toString(), StaffInfo.class);
                logger.info("staff info list size : " + staffInfos.size());
                Set<Long> lableIdSet = new HashSet<>();
                for (StaffInfo staffInfo : staffInfos) {
                    if (staffInfo.getLableId() == null || lableIdSet.contains(staffInfo.getLableId())) {
                        continue;
                    }
                    if (isOverTime(staffInfo.getUpdateTime())) {
                        logger.info("the staff info is over time: " + staffInfo.getId());
                        continue;
                    }
                    LableManage lableManage = getLableInfoById(staffInfo.getLableId());
                    if (lableManage != null && !StringUtils.isEmpty(lableManage.getNumber())) {
                        staffInfo.setLableNumber(lableManage.getNumber());
                    } else {
                        continue;
                    }
                    lableIdSet.add(staffInfo.getLableId());
                    result.add(new LabelUser(staffInfo.getLableNumber(), staffInfo.getPipeUserName(), true,
                            null, staffInfo.getAcceptUserName()));
                }
            }
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return result;
        }
    }

    public Map<String, Object> setParams() {
        Map<String, Object> result = new HashMap<>();
        result.put("page", 1);
        result.put("limit", 50);
        return result;
    }

    public LableManage getLableInfoById(Long id) {
        try {
            JSONObject jsonObject = JSONObject.parseObject(assetsClient.getlableInfo(id));
            if (jsonObject != null && Integer.valueOf(jsonObject.get("code").toString()).equals(0) && jsonObject.containsKey("data")) {
                logger.info("get lable manage info from feign:" + jsonObject.get("data"));
                if (StringUtils.isEmpty(jsonObject.get("data"))) {
                    return null;
                }
                LableManage lableManage = JSONObject.parseObject(jsonObject.get("data").toString(), LableManage.class);
                logger.info(lableManage.toString());
                return lableManage;
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Boolean isOverTime(Date updateTime) {
        Long currentTime = System.currentTimeMillis();
        Long lastTime = updateTime.getTime();
        Long intervalTime = Long.parseLong(interval) * 60 * 60 * 1000L;
        if ((currentTime - lastTime) > intervalTime) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public List<VideoInfo> getVideo(Long capsuleId) {
        List<VideoInfo> result = new ArrayList<>();
        Map<String, Object> params = new HashMap<>();
        params.put("capsule", capsuleId);
        try {
            JSONObject jsonObject = JSONObject.parseObject(deviceClient.getVideoByType(params));
            if (jsonObject != null && Integer.valueOf(jsonObject.get("code").toString()).equals(0) && jsonObject.containsKey("data")) {
                List<JSONObject> deviceJSONList = ParameterUtil.getParameter(jsonObject, "data", List.class);
                for (JSONObject json : deviceJSONList) {
                    VideoInfo videoInfo = JSONObject.parseObject(json.toJSONString(), VideoInfo.class);
                    result.add(videoInfo);
                }
            }
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return result;
        }
    }

    @Override
    public List<Device> getDevice(String type, Long capsuleId) {
        List<Device> result = new ArrayList<>();
        Map<String, Object> params = new HashMap<>();
        params.put("type", type);
        params.put("capsule", capsuleId);
        try {
            JSONObject jsonObject = JSONObject.parseObject(deviceClient.getDeviceByType(params));
            if (jsonObject != null && Integer.valueOf(jsonObject.get("code").toString()).equals(0) && jsonObject.containsKey("data")) {
                List<JSONObject> deviceJSONList = ParameterUtil.getParameter(jsonObject, "data", List.class);
                for (JSONObject json : deviceJSONList) {
                    Device device = JSONObject.parseObject(json.toJSONString(), Device.class);
                    result.add(device);
                }
            }
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return result;
        }
    }

    public static void main(String[] args) {
        String test = "{\n" +
                "  \"data\": null,\n" +
                "  \"code\": 0,\n" +
                "  \"msg\": \"\"\n" +
                "}";
        JSONObject json = JSONObject.parseObject(test);
        System.out.println(StringUtils.isEmpty(json.get("data")));
        System.out.println(json.get("data"));
    }
}
