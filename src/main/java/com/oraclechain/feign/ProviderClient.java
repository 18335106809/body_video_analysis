package com.oraclechain.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/6/12
 */
@FeignClient("ops-center")
public interface ProviderClient {

    @RequestMapping(value = "/ExternalStaffPipe/page", method = RequestMethod.POST)
    String getExStaffInfo(@RequestBody Map<String, Object> params);

    @RequestMapping(value = "/staffpipe/findAllByPage", method = RequestMethod.POST)
    String getStaffInfo(@RequestBody Map<String, Object> params);

}
