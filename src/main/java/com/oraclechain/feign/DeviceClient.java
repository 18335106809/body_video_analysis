package com.oraclechain.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Map;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/9/21
 */
@FeignClient("device-center")
public interface DeviceClient {

    @RequestMapping(value = "/device/listByType", method = RequestMethod.POST)
    String getDeviceByType(@RequestBody Map<String, Object> map);

    @RequestMapping(value = "/VideoInfo/listByType", method = RequestMethod.POST)
    String getVideoByType(@RequestBody Map<String, Object> map);
}
