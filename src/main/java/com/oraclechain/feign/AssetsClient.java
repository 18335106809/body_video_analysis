package com.oraclechain.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/9/9
 */
@FeignClient("assets-center")
public interface AssetsClient {

    @RequestMapping(value = "/LableManage/find", method = RequestMethod.GET)
    String getlableInfo(@RequestParam("id") Long id);
}
