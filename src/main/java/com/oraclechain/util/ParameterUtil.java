package com.oraclechain.util;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/6/12
 */
public class ParameterUtil {

    static ObjectMapper objectMapper = new ObjectMapper();

    public static <T> T getParameter(Map<String, Object> map, String key, Class<T> clazz) {
        T t = null;
        if (map.get(key) != null) {
//            t = JSONObject.parseObject(map.get(key).toString(), clazz);
            t = objectMapper.convertValue(map.get(key), clazz);
        }
        return t;
    }

    public static Integer toArray(String parameter) {
        try {
            if (parameter.startsWith("[") && parameter.endsWith("]")) {
                parameter = parameter.replace("[", "").replace("]", "");
                String[] strings = parameter.split(",");
                return Integer.valueOf(strings[0]);
            } else {
                return 0;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }
}
