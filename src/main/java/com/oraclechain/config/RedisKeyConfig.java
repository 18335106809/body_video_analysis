package com.oraclechain.config;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/1/14
 */
public class RedisKeyConfig {

    public static final String DEVICE_KEY = "device:";
    public static final String DEVICE_PARAMETER_KEY = "device.parameter:";
    public static final String DEVICE_SPACE_KEY= "spaceManage:";
    public static final String DEVICE_HEALTH_KEY = "device.health.";
    public static final String DEVICE_REALTIME_DATA = "device.real.time.data:";
    public static final String DEVICE_MOCK_KEY = "device.mock:";
    public static final String DEVICE_ALARM_KEY = "device.alarm:";
    public static final String DEVICE_INSTRUCTION_KEY = "device.instruction:";
    public static final String USER_POSITION_KEY = "user.position:";
    public static final String DEVICE_CAPSULE_LIGHT_KEY = "device.capsule.light:";
    public static final String POSITION_LOCATION_KEY = "position.location.key";

}
